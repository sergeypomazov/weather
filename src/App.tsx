import { Filter, Head, CurrentWeather, Forecast } from "./components"
import { QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { queryClient } from "./hooks/react-query";

export const App = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <main>
                <Filter />
                <Head />
                <CurrentWeather />
                <Forecast />
            </main>
            <ReactQueryDevtools initialIsOpen />
        </QueryClientProvider>
    );
};
