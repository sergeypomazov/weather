import { render } from 'react-dom';

import { App } from './App';

import './theme/index.scss';

render(<App />, document.getElementById('root'));
