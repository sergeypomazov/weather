import axios from "axios";
import WeatherModel from "../models/WeatherModel";

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export const api = {
    async getWeather() : Promise<WeatherModel[]> {
        const response = await axios.get<{data: WeatherModel[]}>(`${WEATHER_API_URL}`);
        return response.data.data;
    },
};
