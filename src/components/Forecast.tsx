import React from "react";
import { useFetchForecast } from "../hooks/useFetchForecast";
import { ForecastDay } from "./ForecastDay";
import WeatherModel from "../models/WeatherModel";
import weatherStore, { WeatherType } from "../stores/WeatherStore";
import { observer } from "mobx-react-lite";

export const Forecast: React.FC = observer(() => {
    const { isLoading, data } = useFetchForecast();

    if (isLoading) {
        return <h2>Loading...</h2>
    }

    if (data && !weatherStore.selectedDay) {
        weatherStore.setSelectedDay(data[0]);
    }

    const daysList = data?.slice(0, 7).map((weather: WeatherModel) =>
        <ForecastDay key={weather.id} weather={weather} />
    );

    return (
        <div className="forecast">
            {daysList}
        </div>
    );
});