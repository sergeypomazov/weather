import React from "react";
import weatherStore from "../stores/WeatherStore";
import { observer } from "mobx-react-lite";

export const CurrentWeather: React.FC<{}> = observer(() => {
    const selectedDay = weatherStore.getSelectedDay();
    return (
        <div className="current-weather">
            <p className="temperature">{selectedDay && selectedDay.temperature.toFixed()}</p>
            <p className="meta">
                <span className="rainy">%{selectedDay && selectedDay.rain_probability.toFixed()}</span>
                <span className="humidity">%{selectedDay && selectedDay.humidity.toFixed()}</span>
            </p>
        </div>
    );
});