import React from "react";
import days from "../types/days";
import weatherStore from "../stores/WeatherStore";
import WeatherModel from "../models/WeatherModel";
import { observer } from "mobx-react-lite";
const classnames = require("classnames");

interface ForecastDayProps {
    weather: WeatherModel
}

export const ForecastDay: React.FC<ForecastDayProps> = observer((props) => {
    const { weather } = props;
    const date = new Date(weather.day);
    const selectedDay = weatherStore.getSelectedDay();

    return (
        <div onClick={() => weatherStore.setSelectedDay(weather)}
             className={classnames('day', weather.type, {'selected': selectedDay?.id == weather.id})}
        >
            <p>{days[date.getDay()]}</p>
            <span>{weather.temperature.toFixed()}</span>
        </div>
    );
});