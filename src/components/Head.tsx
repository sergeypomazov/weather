import React, { useContext } from "react";
import days from "../types/days";
import months from '../types/months';
import weatherStore from "../stores/WeatherStore";
import { observer } from "mobx-react-lite";
const classnames = require("classnames");

interface HeadProps {}

export const Head: React.FC<HeadProps> = observer(() => {
    const weather = weatherStore.getSelectedDay();
    const date = weather ? new Date(weather.day) : null;

    return (
        <div className="head">
            <div className={classnames('icon', weather?.type)}></div>
            <div className="current-date">
                <p>{date && days[date.getDay()]}</p>
                <span>{date && `${date.getDate()} ${months[date.getMonth()]}`}</span>
            </div>
        </div>
    );
});
