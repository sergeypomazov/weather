type WeatherType = 'rainy' | 'sunny' | 'cloudy';

interface WeatherModel {
    id:	string;
    rain_probability: number;
    humidity: number;
    day: number;
    temperature: number;
    type: WeatherType;
}

export default WeatherModel;