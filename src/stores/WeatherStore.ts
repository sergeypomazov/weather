import { Instance, types } from "mobx-state-tree";

export type WeatherType = Instance<typeof Weather>;

const Weather = types
    .model('Weather', {
        id:	types.string,
        rain_probability: types.number,
        humidity: types.number,
        day: types.number,
        temperature: types.number,
        type: types.string
    });

const WeatherStore = types
    .model('WeatherStore', {
        selectedDay: types.maybe(Weather)
    })
    .actions(self => ({
        setSelectedDay(weather: WeatherType) {
            self.selectedDay = Object.assign({}, weather);
        }
    }))
    .views(self => ({
        getSelectedDay(): WeatherType | undefined {
            return self.selectedDay;
        }
    }));


const weatherStore = WeatherStore.create({
    selectedDay: undefined
});

export default weatherStore;