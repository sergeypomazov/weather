import {useQuery} from "react-query";
import {api} from "../api";

export const useFetchForecast = () => {
    const query = useQuery('weather', api.getWeather);

    return query;
}